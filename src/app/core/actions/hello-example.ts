/* tslint:disable:max-classes-per-file */

// This file is used into ../../app.module.ts

import { Action } from '@ngrx/store';

export const SAY_HELLO = '[Example] Say Hello';
export const SAY_BYEBYE = '[Example] Say ByeBye';

export class SayHelloAction implements Action {
  readonly type = SAY_HELLO;
}

export class SayByeByeAction implements Action {
  readonly type = SAY_BYEBYE;
}

export type Actions = SayHelloAction | SayByeByeAction;
