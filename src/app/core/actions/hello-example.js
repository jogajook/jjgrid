"use strict";
/* tslint:disable:max-classes-per-file */
exports.__esModule = true;
exports.SAY_HELLO = '[Example] Say Hello';
exports.SAY_BYEBYE = '[Example] Say ByeBye';
var SayHelloAction = /** @class */ (function () {
    function SayHelloAction() {
        this.type = exports.SAY_HELLO;
    }
    return SayHelloAction;
}());
exports.SayHelloAction = SayHelloAction;
var SayByeByeAction = /** @class */ (function () {
    function SayByeByeAction() {
        this.type = exports.SAY_BYEBYE;
    }
    return SayByeByeAction;
}());
exports.SayByeByeAction = SayByeByeAction;
