/* tslint:disable:max-classes-per-file variable-name */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class GithubUser {
  constructor(
    public login: string,
    public id: number,
    public url: string,
    public repos_url: string,
    public name: string | void,
    public company: string | void,
    public location: string | void
  ) {}
}

export class GithubOrg {
  constructor(
    public login: string | void,
    public id: number | void,
    public url: string | void,
    public repos_url: string | void,
    public events_url: string | void,
    public hooks_url: string | void,
    public issues_url: string | void,
    public members_url: string | void,
    public public_members_url: string | void,
    public avatar_url: string | void,
    public description: string | void
  ) {}
}

/**
 * Example of a service to retrieve remote data from https://api.github.com/users/Ks89
 */
@Injectable()
export class GithubService {
  constructor(private httpClient: HttpClient) {}

  /**
   * Method to get my Github profile asynchronously using Github's apis.
   * @returns A Observable<GithubUser> with data inside.
   */
  getGithubUser(): Observable<GithubUser> {
    return this.httpClient.get<GithubUser>('https://api.github.com/users/Ks89');
  }

  /**
   * Method to get all organizations of my Github profile asynchronously using Github's apis.
   * @returns A Observable<GithubUser> with data inside.
   */
  getGithubKs89Organizations(): Observable<GithubOrg> {
    return this.httpClient.get<GithubOrg>(
      'https://api.github.com/users/Ks89/orgs'
    );
  }
}
