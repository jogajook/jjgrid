"use strict";
/* tslint:disable:max-classes-per-file variable-name */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var GithubUser = /** @class */ (function () {
    function GithubUser(login, id, url, repos_url, name, company, location) {
        this.login = login;
        this.id = id;
        this.url = url;
        this.repos_url = repos_url;
        this.name = name;
        this.company = company;
        this.location = location;
    }
    return GithubUser;
}());
exports.GithubUser = GithubUser;
var GithubOrg = /** @class */ (function () {
    function GithubOrg(login, id, url, repos_url, events_url, hooks_url, issues_url, members_url, public_members_url, avatar_url, description) {
        this.login = login;
        this.id = id;
        this.url = url;
        this.repos_url = repos_url;
        this.events_url = events_url;
        this.hooks_url = hooks_url;
        this.issues_url = issues_url;
        this.members_url = members_url;
        this.public_members_url = public_members_url;
        this.avatar_url = avatar_url;
        this.description = description;
    }
    return GithubOrg;
}());
exports.GithubOrg = GithubOrg;
/**
 * Example of a service to retrieve remote data from https://api.github.com/users/Ks89
 */
var GithubService = /** @class */ (function () {
    function GithubService(httpClient) {
        this.httpClient = httpClient;
    }
    /**
     * Method to get my Github profile asynchronously using Github's apis.
     * @returns A Observable<GithubUser> with data inside.
     */
    GithubService.prototype.getGithubUser = function () {
        return this.httpClient.get('https://api.github.com/users/Ks89');
    };
    /**
     * Method to get all organizations of my Github profile asynchronously using Github's apis.
     * @returns A Observable<GithubUser> with data inside.
     */
    GithubService.prototype.getGithubKs89Organizations = function () {
        return this.httpClient.get('https://api.github.com/users/Ks89/orgs');
    };
    GithubService = __decorate([
        core_1.Injectable()
    ], GithubService);
    return GithubService;
}());
exports.GithubService = GithubService;
