"use strict";
exports.__esModule = true;
var example_service_1 = require("./example.service");
var example_service_2 = require("./example.service");
exports.ExampleService = example_service_2.ExampleService;
var github_service_1 = require("./github.service");
var github_service_2 = require("./github.service");
exports.GithubService = github_service_2.GithubService;
/**
 * Array of core services for the app SPA
 */
exports.CORE_SERVICES = [
    example_service_1.ExampleService,
    github_service_1.GithubService
];
