/* tslint:disable:max-classes-per-file */

// This file is used into ../../app.module.ts

import * as example from '../actions/hello-example';

export interface State {
  message: string;
}

const initialState: State = {
  message: ''
};

export function reducer(state = initialState, action: example.Actions): State {
  switch (action.type) {
    case example.SAY_BYEBYE:
      return {
        message: 'bye bye!'
      };

    case example.SAY_HELLO:
      return {
        message: 'hello!'
      };

    default:
      return state;
  }
}

export const getHelloExample = (state: State) => state.message;
