"use strict";
/* tslint:disable:max-classes-per-file */
exports.__esModule = true;
// This file is used into ../../app.module.ts
var example = require("../actions/hello-example");
var initialState = {
    message: ''
};
function reducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case example.SAY_BYEBYE:
            return {
                message: 'bye bye!'
            };
        case example.SAY_HELLO:
            return {
                message: 'hello!'
            };
        default:
            return state;
    }
}
exports.reducer = reducer;
exports.getHelloExample = function (state) { return state.message; };
