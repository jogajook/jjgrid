import { Component } from '@angular/core';

@Component({
  selector: 'jj-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng7';
}
