import { Routes } from '@angular/router';
import { JJGridExComponent } from './jj-grid/jj-grid-ex.component';

export const ROUTES: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/grid' },
  { path: 'grid', component: JJGridExComponent }
];
