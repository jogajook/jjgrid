"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var components_1 = require("../../shared/components/components");
var reducers_1 = require("../../shared/components/jj-grid/reducers");
var Observable_1 = require("rxjs/Observable");
var models_1 = require("../../shared/components/jj-grid/models");
var JJGridExComponent = /** @class */ (function () {
    function JJGridExComponent(http) {
        this.http = http;
        this.subscriptions = [];
        this.pageHeader = new components_1.PageHeader('JJ Grid Example', '');
        var columns = [
            {
                prop: 'name',
                title: 'Name'
            },
            {
                prop: 'description',
                title: 'Description'
            }
        ];
        this.dataSource = new DevelopersDataSource(columns, null, http);
    }
    JJGridExComponent.prototype.onGridReady = function (store) {
        this.clickedCell$ = store.select(reducers_1.selectors.getClickedCell);
        this.subscriptions.push(this.clickedCell$.subscribe(function (clickedCell) {
            if (clickedCell) {
                console.log('1111', clickedCell);
            }
        }));
    };
    JJGridExComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (s) { return s.unsubscribe(); });
    };
    JJGridExComponent = __decorate([
        core_1.Component({
            selector: 'jj-grid-ex',
            templateUrl: 'jj-grid-ex.html',
            styleUrls: ['jj-grid-ex.scss']
        })
    ], JJGridExComponent);
    return JJGridExComponent;
}());
exports.JJGridExComponent = JJGridExComponent;
var DevelopersDataSource = /** @class */ (function (_super) {
    __extends(DevelopersDataSource, _super);
    function DevelopersDataSource(columns, params, http) {
        var _this = _super.call(this, columns, params) || this;
        _this.columns = columns;
        _this.params = params;
        _this.http = http;
        _this.loadData();
        return _this;
    }
    DevelopersDataSource.prototype.getData = function (params) {
        return Observable_1.Observable.of({
            page: 1,
            totalItems: 120,
            items: [
                {
                    id: '1',
                    name: 'name 1',
                    description: 'description 1'
                },
                {
                    id: '2',
                    name: 'name 2',
                    description: 'description 2'
                }
            ]
        });
        // return this.http.post<IData>('http://localhost:3000/api/developers', this.params);
    };
    ;
    return DevelopersDataSource;
}(models_1.DataSource));
