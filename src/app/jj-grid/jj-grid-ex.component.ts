import { Component, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GridExampleDataSource } from './data-source';
import {
  ColumnFactory,
  EColumnType,
  IInvokedItemAction,
  CellClickedAction,
  IGridAction,
  ItemActionInvokedAction,
  ICellClickEvent,
  PublicGridActionType,
  Params,
  IFilterConfig,
  EFilterType
} from '../shared/modules/jj-grid';
import { IGridConfig } from '../shared/modules/jj-grid/models/i-grid-config';
import { of } from 'rxjs';

@Component({
  selector: 'jj-grid-ex',
  templateUrl: 'jj-grid-ex.html',
  styles: ['.jj-grid-cell_status { color: #36e036; }'],
  encapsulation: ViewEncapsulation.None
})
export class JJGridExComponent {
  dataSource: GridExampleDataSource;
  private gridItemActionNames = {
    edit: 'EDIT',
    open: 'OPEN'
  };

  constructor(private http: HttpClient) {
    const columns = [
      ColumnFactory.create(EColumnType.String, {
        prop: 'id',
        title: 'Id',
        isSortable: true
      }),

      ColumnFactory.create(EColumnType.String, {
        prop: 'name',
        title: 'Name',
        isSortable: true
      }),

      ColumnFactory.create(EColumnType.Status, {
        prop: 'status',
        title: 'Status',
        isSortable: false
      }),

      ColumnFactory.create(EColumnType.Actions, {
        title: 'Actions',
        actionConfig: {
          getFilter: (item: any): boolean => {
            return true;
          },
          actions: [
            {
              name: this.gridItemActionNames.edit
            },
            {
              name: this.gridItemActionNames.open
            }
          ]
        }
      })
    ];

    const gridConfig: IGridConfig = {
      isSelectable: true,
      filterConfig: [
        {
          prop: columns[1].prop,
          type: EFilterType.MultiSelect,
          optionsProvider: {
            // url: '/filter-options',
            factory: (s, f) => {
              const opt1  = new Date().getTime();
              const opt2  = new Date().getTime() + 1;
              return of([
                {
                  id: opt1,
                  name: `Option - ${opt1}`
                },
                {
                  id: opt2,
                  name: `Option - ${opt2}`
                }
              ]);
            }
          }
        }
      ]
    };

    this.dataSource = new GridExampleDataSource(
      columns,
      undefined,
      undefined,
      gridConfig,
      http
    );
  }

  onGridAction(action: IGridAction): void {
    switch (action.type) {
      case PublicGridActionType.ItemActionInvoked:
        this.onItemActionInvoked((<ItemActionInvokedAction>action).payload);
        break;

      case PublicGridActionType.CellClicked:
        this.onCellClikced((<CellClickedAction>action).payload);
        break;

      default:
        break;
    }
  }

  private onItemActionInvoked(invokedItemAction: IInvokedItemAction): void {
    switch (invokedItemAction.action.name) {
      case this.gridItemActionNames.edit:
        console.log('editing item', invokedItemAction.item);
        break;
      case this.gridItemActionNames.open:
        console.log('opening item', invokedItemAction.item);
        break;
      default:
        console.warn('Unknown grid item action called', invokedItemAction);
    }
  }

  private onCellClikced(cellClickEvent: ICellClickEvent): void {
    console.log(
      'clicked column: ',
      cellClickEvent.column,
      'grid item: ',
      cellClickEvent.item
    );
  }
}
