import { IParams } from '../shared/modules/jj-grid/models/i-params';
import { Observable, of } from 'rxjs';
import { DataSource } from '../shared/modules/jj-grid/models/data-source';
import { HttpClient } from '@angular/common/http';
import { IData } from '../shared/modules/jj-grid/models/i-data';
import { IColumn } from '../shared/modules/jj-grid/models/i-column';
import { IExampleGridItem } from './i-exampe-grid-item';

export class GridExampleDataSource extends DataSource<IExampleGridItem> {
  constructor(
    public columns: IColumn[],
    params: IParams,
    public id: string = new Date().getTime().toString(),
    public config: any = {},
    private http: HttpClient
  ) {
    super(columns, params, id, config);
  }

  getData(params: IParams): Observable<IData<IExampleGridItem>> {
    console.log('getting data for params', params);
    const data = {
      page: 2,
      totalItems: 133,
      items: []
    };
    for (let index = 0; index < 20; index++) {
      data.items.push({
        id: String(++index),
        name: `name ${index}`,
        status: `Status_${index}`
      });
    }

    return of(data);
    // return this.http.post<IData>('http://localhost:3000/api/someApi', this.params);
  }
}
