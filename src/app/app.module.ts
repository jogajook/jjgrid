import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { JJGridModule } from './shared/modules/jj-grid/jj-grid.module';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { ROUTES } from './app.routing';
import { JJGridExComponent } from './jj-grid/jj-grid-ex.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent, JJGridExComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules
    }),
    SharedModule,
    JJGridModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
