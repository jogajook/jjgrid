import { IDataSource } from '../models/i-data-source';
import { ICell } from '../models/i-cell';

export interface JJGridState {
  clickedCell: ICell;
  invokedItemAction: any;
  selectedItems: any[];
  dataSource: IDataSource;
}
