import { JJGridState } from './i-grid-state';
import { IParams } from '../models/i-params';
import { IDataSource } from '../models/i-data-source';
import { createSelector } from '@ngrx/store';
import * as actions from './grid-actions';
import { initialState } from './initial-state';
import { GridActionType } from '../models/e-grid-action-type';
import Utils from '../models/utils';
import { Params } from '../models/params';
import { JJGridMapState } from './i-grid-map-state';
import { IData } from '../models/i-data';
import { IGridSelectors } from './i-grid-selectors';
import { IGridAction } from '../models';
import { initialGridState } from './initial-grid-state';
import { IFilterChangedEvent } from '../models/i-filter-changed-event';

export function reducer(
  gridMapState: JJGridMapState = initialState,
  action: IGridAction
): JJGridMapState {
  let dataSource: IDataSource;

  const gridState: JJGridState =
    gridMapState[action.gridId] || initialGridState;
  let updatedState: JJGridState;

  switch (action.type) {
    case GridActionType[GridActionType.DataSourceReady]:
      updatedState = Object.assign({}, gridState, {
        dataSource: action.payload
      });
      updatedState.dataSource.params.filters = updatedState.dataSource.config.filterConfig.map(
        conf => ({
          prop: conf.prop,
          data: []
        })
      );
      break;

    case GridActionType[GridActionType.CellClicked]:
      updatedState = Object.assign({}, gridState, {
        clickedCell: action.payload
      });
      break;

    case GridActionType[GridActionType.ItemActionInvoked]:
      updatedState = Object.assign({}, gridState, {
        invokedItemAction: action.payload
      });
      break;

    case GridActionType[GridActionType.SearchChanged]:
      dataSource = Utils.copyInstance(gridState.dataSource);
      const params1 = dataSource.params;
      dataSource.params = new Params(
        params1.sortBy,
        params1.sortOrder,
        action.payload,
        undefined,
        params1.pageSize,
        params1.filters
      );
      updatedState = { ...gridState, dataSource };
      break;

    case GridActionType[GridActionType.SortingChanged]:
      dataSource = Utils.copyInstance(gridState.dataSource);
      const params2 = dataSource.params;
      dataSource.params = new Params(
        action.payload.column,
        action.payload.sorting,
        params2.search,
        undefined,
        params2.pageSize,
        params2.filters
      );
      updatedState = { ...gridState, dataSource };
      break;

    case GridActionType[GridActionType.PageChanged]:
      dataSource = Utils.copyInstance(gridState.dataSource);
      const params3 = dataSource.params;
      dataSource.params = new Params(
        params3.sortBy,
        params3.sortOrder,
        params3.search,
        action.payload.pageNumber,
        params3.pageSize,
        params3.filters
      );
      updatedState = { ...gridState, dataSource };
      break;

    case GridActionType[GridActionType.SelectAllItemsChanged]:
      const selectedItems1 = action.payload
        ? [...gridState.dataSource.data.items]
        : [];
      updatedState = { ...gridState, selectedItems: selectedItems1 };
      break;

    case GridActionType[GridActionType.ToggleItemSelected]:
      const selectedItems2 = action.payload.isSelected
        ? [...gridState.selectedItems, action.payload.item]
        : gridState.selectedItems.filter(
            si => si.id !== action.payload.item.id
          );
      updatedState = { ...gridState, selectedItems: selectedItems2 };
      break;

    case GridActionType[GridActionType.FilterChanged]:
      dataSource = Utils.copyInstance(gridState.dataSource);
      const filterChangedEvent = action.payload as IFilterChangedEvent;
      const params4 = dataSource.params;
      const filters = [
        ...params4.filters.filter(f => f.prop !== filterChangedEvent.filter.prop),
        filterChangedEvent.filter
      ];

      let sortBy = params4.sortBy;
      let sortOrder = params4.sortOrder;
      if (typeof filterChangedEvent.sorting !== 'undefined') {
        sortBy = filterChangedEvent.column;
        sortOrder = filterChangedEvent.sorting;
      }

      dataSource.params = new Params(
        sortBy,
        sortOrder,
        params4.search,
        undefined,
        params4.pageSize,
        filters
      );
      updatedState = { ...gridState, dataSource };
      break;

    default:
      return gridMapState;
  }

  const newState: JJGridMapState = {
    ...gridMapState,
    [action.gridId]: updatedState
  };

  console.log('action', action);
  console.log('state', updatedState);
  console.log('\n');

  return newState;
}

const getClickedCell = (state: JJGridState) => state.clickedCell;
const getInvokedItemAction = (state: JJGridState) => state.invokedItemAction;
const getSelectedItems = (state: JJGridState) => state.selectedItems;
const getDataSource = (state: JJGridState) => state.dataSource;
const getDataSourceColumns = (dataSource: IDataSource) => dataSource.columns;
const getDataSourceParams = (dataSource: IDataSource) => dataSource.params;
const getDataSourceConfig = (dataSource: IDataSource) => dataSource.config;
const getDataSourceData = (dataSource: IDataSource) => dataSource.data;
const getDataSourceDataTotalItems = (data: IData) => data && data.totalItems;
const getDataSourceDataItems = (data: IData) => data && data.items;
const getParamsSearch = (params: IParams) => params.search;
const getParamsPageSize = (params: IParams) => params.pageSize;
const getParamsPageNumber = (params: IParams) => params.pageNumber;

const getColumns = createSelector(
  getDataSource,
  getDataSourceColumns
);

const getParams = createSelector(
  getDataSource,
  getDataSourceParams
);

const getConfig = createSelector(
  getDataSource,
  getDataSourceConfig
);

const getPageSize = createSelector(
  getParams,
  getParamsPageSize
);

const getPageNumber = createSelector(
  getParams,
  getParamsPageNumber
);

const getSearch = createSelector(
  getParams,
  getParamsSearch
);

const getData = createSelector(
  getDataSource,
  getDataSourceData
);

const getDataTotalItems = createSelector(
  getData,
  getDataSourceDataTotalItems
);

const getDataItems = createSelector(
  getData,
  getDataSourceDataItems
);

const getSelectedAllItems = createSelector(
  getSelectedItems,
  getDataItems,
  (selectedItems, items) => selectedItems.length === items.length
);

export const gridSelectors: IGridSelectors = {
  getClickedCell,
  getInvokedItemAction,
  getSelectedItems,
  getSelectedAllItems,
  getDataSource,
  getColumns,
  getParams,
  getConfig,
  getPageSize,
  getPageNumber,
  getSearch,
  getData,
  getDataTotalItems,
  getDataItems
};
