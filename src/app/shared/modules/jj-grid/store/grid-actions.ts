/* tslint:disable:max-classes-per-file */

import { IDataSource } from '../models/i-data-source';
import { IInvokedItemAction } from '../models/i-invoked-item-action';
import { GridAction } from '../models/grid-action';
import { GridActionType } from '../models/e-grid-action-type';
import { ISortingChangeEvent } from '../models/i-sorting-change-event';
import { IPageChangedEvent } from '../models/i-page-changed-event';
import { ICellClickEvent } from '../models/i-cell-click-event';
import { IToggleItemSelectedEvent } from '../models';
import { IFilter } from '../models/i-filter';
import { IFilterChangedEvent } from '../models/i-filter-changed-event';

/** PUBLIC ACTIONS */

export class CellClickedAction extends GridAction {
  readonly type = GridActionType.CellClicked;

  constructor(public payload: ICellClickEvent, public gridId: string) {
    super();
  }
}

export class ItemActionInvokedAction extends GridAction {
  readonly type = GridActionType.ItemActionInvoked;

  constructor(public payload: IInvokedItemAction, public gridId: string) {
    super();
  }
}


/** PRIVATE ACTIONS */

export class DataSourceReadyAction extends GridAction {
  readonly type = GridActionType.DataSourceReady;

  constructor(public payload: IDataSource, public gridId: string) {
    super();
  }
}

export class SortingChangedAction extends GridAction {
  readonly type = GridActionType.SortingChanged;

  constructor(public payload: ISortingChangeEvent, public gridId: string) {
    super();
  }
}

export class SearchChangedAction extends GridAction {
  readonly type = GridActionType.SearchChanged;

  constructor(public payload: string, public gridId: string) {
    super();
  }
}

export class PageChangedAction extends GridAction {
  readonly type = GridActionType.PageChanged;

  constructor(public payload: IPageChangedEvent, public gridId: string) {
    super();
  }
}
export class SelectAllItemsChangedAction extends GridAction {
  readonly type = GridActionType.SelectAllItemsChanged;

  constructor(public payload: boolean, public gridId: string) {
    super();
  }
}
export class ToggleItemSelectedAction extends GridAction {
  readonly type = GridActionType.ToggleItemSelected;

  constructor(public payload: IToggleItemSelectedEvent, public gridId: string) {
    super();
  }
}

export class PageSizeChangedAction extends GridAction {
  readonly type = GridActionType.PageSizeChanged;

  constructor(public payload: { pageSize: number }, public gridId: string) {
    super();
  }
}
export class FilterChangedAction extends GridAction {
  readonly type = GridActionType.FilterChanged;

  constructor(public payload: IFilterChangedEvent, public gridId: string) {
    super();
  }
}

export type GridActions =
    CellClickedAction
  | DataSourceReadyAction
  | SortingChangedAction
  | SearchChangedAction
  | ItemActionInvokedAction
  | PageChangedAction
  | PageSizeChangedAction;
