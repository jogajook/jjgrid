export interface IGridSelectors {
  getClickedCell: any;
  getInvokedItemAction: any;
  getSelectedItems: any;
  getSelectedAllItems: any;
  getDataSource: any;
  getColumns: any;
  getData: any;
  getDataTotalItems: any;
  getDataItems: any;
  getParams: any;
  getConfig: any;
  getPageSize: any;
  getPageNumber: any;
  getSearch: any;
}
