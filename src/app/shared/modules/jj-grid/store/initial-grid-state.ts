import { JJGridState } from './i-grid-state';

export const initialGridState: JJGridState = {
  clickedCell: null,
  invokedItemAction: null,
  selectedItems: [],
  dataSource: null
};
