import { State } from './i-state';
import { ActionReducerMap, createSelector } from '@ngrx/store';
import * as fromGridReducer from './grid-reducer';
import { JJGridMapState } from './i-grid-map-state';
import { IGridSelectors } from './i-grid-selectors';

export const reducers: ActionReducerMap<any> = {
  jjGrid: fromGridReducer.reducer
};

export const getGridMapState = (state: State): JJGridMapState => state.jjGrid;
export const getSpecificGridStateSelectors = (gridId: string): IGridSelectors => {

    const getGridState = createSelector(
      getGridMapState,
      (state: JJGridMapState) => state[gridId]
    );

    const getClickedCell = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getClickedCell,
    );

    const getInvokedItemAction = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getInvokedItemAction,
    );

    const getSelectedItems = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getSelectedItems,
    );

    const getSelectedAllItems = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getSelectedAllItems,
    );

    const getDataSource = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getDataSource,
    );

    const getColumns = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getColumns,
    );

    const getData = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getData,
    );

    const getDataTotalItems = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getDataTotalItems,
    );

    const getDataItems = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getDataItems,
    );

    const getParams = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getParams,
    );

    const getConfig = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getConfig,
    );

    const getPageSize = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getPageSize
    );

    const getPageNumber = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getPageNumber
    );

    const getSearch = createSelector(
      getGridState,
      fromGridReducer.gridSelectors.getSearch,
    );

    const selectors: IGridSelectors = {
      getClickedCell,
      getInvokedItemAction,
      getSelectedItems,
      getSelectedAllItems,
      getDataSource,
      getColumns,
      getData,
      getDataTotalItems,
      getDataItems,
      getParams,
      getConfig,
      getPageSize,
      getPageNumber,
      getSearch
    };

    return selectors;
};

