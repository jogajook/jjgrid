import { JJGridMapState } from './i-grid-map-state';

export interface State {
  jjGrid: JJGridMapState;
}
