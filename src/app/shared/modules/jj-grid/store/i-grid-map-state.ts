import { JJGridState } from '.';

export interface JJGridMapState {
  [gridId: string]: JJGridState;
}
