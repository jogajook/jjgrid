import {
  Component,
  Input,
  ChangeDetectionStrategy,
  EventEmitter,
  Output
} from '@angular/core';
import { IColumn } from '../../models/i-column';
import { EColumnType } from '../../models/e-column-type';
import { ICellClickEvent } from '../../models/i-cell-click-event';
import { IInvokedItemActionEvent } from '../../models/i-invoked-item-action-event';

@Component({
  selector: 'jj-grid-cell',
  templateUrl: 'cell.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JJGridCellComponent {
  @Input() column: IColumn;
  @Input() item: any;
  @Output() cellClick: EventEmitter<ICellClickEvent> = new EventEmitter<ICellClickEvent>();
  @Output() itemActionInvoke: EventEmitter<IInvokedItemActionEvent> = new EventEmitter<
  IInvokedItemActionEvent
  >();

  columnType = EColumnType;

  constructor() {}

  onCellClick(item: any, column: IColumn) {
    this.cellClick.emit({ item, column });
  }

  onActionClick(invokedItemActionEvent: IInvokedItemActionEvent) {
    this.itemActionInvoke.emit(invokedItemActionEvent);
  }
}
