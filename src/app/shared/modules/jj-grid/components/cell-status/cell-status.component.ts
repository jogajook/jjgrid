import {
  Component,
  Input,
  OnInit,
  ChangeDetectionStrategy
} from '@angular/core';
import { IColumn } from '../../models/i-column';

@Component({
  selector: 'jj-grid-cell-status',
  templateUrl: 'cell-status.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JJGridCellStatusComponent implements OnInit {
  @Input() column: IColumn;
  @Input() item: any;

  constructor() {}

  ngOnInit() {}
}
