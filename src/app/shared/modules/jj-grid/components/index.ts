import { JJGridComponent } from './grid/grid.component';
import { JJGridPaginationComponent } from './pagination/pagination.component';
import { JJGridColumnComponent } from './column/column.component';
import { JJGridSearchComponent } from './search/search.component';
import { JJGridCellStringComponent } from './cell-string/cell-string.component';
import { JJGridCellComponent } from './cell/cell.component';
import { JJGridCellStatusComponent } from './cell-status/cell-status.component';
import { JJGridCellActionsComponent } from './cell-actions/cell-actions.component';
import { JJGridFilterComponent } from './filter/filter.component';
import { JJGridFitlerMultiSelectComponent } from './filter-multi-select/filter-multi-select.component';

export const JJGRID_COMPONENTS = [
  JJGridComponent,
  JJGridPaginationComponent,
  JJGridColumnComponent,
  JJGridSearchComponent,
  JJGridCellComponent,
  JJGridCellStringComponent,
  JJGridCellStatusComponent,
  JJGridCellActionsComponent,
  JJGridFilterComponent,
  JJGridFitlerMultiSelectComponent,
];
