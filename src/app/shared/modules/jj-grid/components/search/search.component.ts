import {
  Component,
  OnDestroy,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output
} from '@angular/core';

import { Subscription, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'jj-grid-search',
  templateUrl: 'search.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JJGridSearchComponent implements OnInit, OnDestroy {
  @Input() search: string;
  @Output() searchChange: EventEmitter<string> = new EventEmitter<string>();

  private subscriptions: Subscription[] = [];
  private searchSubject$: Subject<string> = new Subject<string>();
  private debounceMs: number = 500;

  constructor() {}

  ngOnInit() {
    this.subscriptions.push(
      this.searchSubject$
        .pipe(debounceTime(this.debounceMs))
        .subscribe((search: string) => {
          this.searchChange.emit(search);
        })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onSearchChange(search: string) {
    this.searchSubject$.next(search);
  }
}
