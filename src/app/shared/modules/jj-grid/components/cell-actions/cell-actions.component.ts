import {
  Component,
  Input,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from '@angular/core';
import { IColumnActions } from '../../models/i-column-actions';
import { IColumn } from '../../models/i-column';
import { IInvokedItemActionEvent } from '../../models/i-invoked-item-action-event';

@Component({
  selector: 'jj-grid-cell-actions',
  templateUrl: 'cell-actions.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JJGridCellActionsComponent implements OnInit {
  @Input() column: IColumnActions;
  @Input() item: any;
  @Output() actionClick: EventEmitter<IInvokedItemActionEvent> = new EventEmitter<IInvokedItemActionEvent>();

  constructor() {}

  private actionFilter: (item: any) => boolean;

  ngOnInit() {
    this.actionFilter = this.column.actionConfig.getFilter;
  }

  isActionAvailable(item: any, column: IColumn, action: any): boolean {
    return this.actionFilter(item);
  }

  onActionClick(action: any, item: any): void {
    this.actionClick.emit({ action, item });
  }
}
