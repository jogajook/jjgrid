import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ChangeDetectionStrategy
} from '@angular/core';
import { Subscription, Observable, combineLatest } from 'rxjs';
import { IDataSource } from '../../models/i-data-source';
import { GridService } from '../../services/grid.service';
import { IParams } from '../../models/i-params';
import { ISortingChangeEvent } from '../../models/i-sorting-change-event';
import { ICellClickEvent } from '../../models/i-cell-click-event';
import { IInvokedItemAction } from '../../models/i-invoked-item-action';
import { IPageChangedEvent } from '../../models/i-page-changed-event';
import { filter, tap, map, take } from 'rxjs/operators';
import { IColumn } from '../../models/i-column';
import { IGridAction } from '../../models/i-grid-action';
import { IGridConfig } from '../../models/i-grid-config';
import { IFilterConfig } from '../..';
import { IFilter } from '../../models/i-filter';
import { IFilterChangedEvent } from '../../models/i-filter-changed-event';

@Component({
  selector: 'jj-grid',
  templateUrl: 'grid.html',
  providers: [GridService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JJGridComponent implements OnInit, OnDestroy {
  @Input() private dataSource: IDataSource;
  @Output() private actions: EventEmitter<IGridAction> = new EventEmitter<
    IGridAction
  >();

  private subscriptions: Subscription[] = [];
  dataSource$: Observable<IDataSource>;
  columns$: Observable<IColumn[]>;
  params$: Observable<IParams>;
  config$: Observable<IGridConfig>;
  search$: Observable<string>;
  pageSize$: Observable<number>;
  dataTotalItems$: Observable<number>;
  pageNumber$: Observable<number>;
  dataItems$: Observable<any>;
  selectedItems$: Observable<any[]>;
  selectedAllItems$: Observable<boolean>;
  selectedItems: any[];
  isSelectedAllItemsIndeterminate$: Observable<boolean>;
  isSelectedAllChecked$: Observable<boolean>;
  filterConfig$: Observable<IFilterConfig[]>;

  constructor(private jJGridService: GridService) {}

  ngOnInit(): void {
    this.jJGridService.registerGrid(this.dataSource);

    this.dataSource$ = this.jJGridService.getDataSource();
    this.columns$ = this.jJGridService.getColumns();
    this.params$ = this.jJGridService.getParams();
    this.search$ = this.jJGridService.getSearch();
    this.pageNumber$ = this.jJGridService.getPageNumber();
    this.pageSize$ = this.jJGridService.getPageSize();
    this.config$ = this.jJGridService.getConfig();
    this.dataTotalItems$ = this.jJGridService.getDataTotalItems();
    this.dataItems$ = this.jJGridService.getDataItems();
    this.selectedItems$ = this.jJGridService.getSelectedItems();
    this.selectedAllItems$ = this.jJGridService.getSelectedAllItems();
    this.isSelectedAllItemsIndeterminate$ = this.jJGridService.isSelectedAllItemsIndeterminate();
    this.isSelectedAllChecked$ = combineLatest(
      this.selectedAllItems$,
      this.isSelectedAllItemsIndeterminate$,
      (sa, intermediate) => sa || intermediate
    );
    this.filterConfig$ = this.jJGridService.getFilterConfig();

    this.subscriptions.push(
      this.jJGridService.getPublicActions().subscribe(action => {
        this.actions.emit(action);
      }),

      this.dataSource$
        .pipe(
          filter((dataSource: IDataSource | null) => !!dataSource),
          tap((dataSource: IDataSource) => {
            dataSource.loadData();
          })
        )
        .subscribe(),

      this.selectedItems$.subscribe(
        selectedItems => (this.selectedItems = selectedItems)
      )
    );
  }

  onSearchChanged(search: string): void {
    this.jJGridService.onSearchChange(search);
  }

  onSortingChanged(sortingChangeEvent: ISortingChangeEvent): void {
    this.jJGridService.onSortingChange(sortingChangeEvent);
  }

  onGridCellClick(cellClickEvent: ICellClickEvent): void {
    this.jJGridService.onCellClick(cellClickEvent);
  }

  onGridInvokedAction(invokedItemAction: IInvokedItemAction): void {
    this.jJGridService.onInvokedAction(invokedItemAction);
  }

  onPageChanged(pageChangedEvent: IPageChangedEvent): void {
    this.jJGridService.onPageChanged(pageChangedEvent);
  }

  onSelectAllItemsChanged(selectedAllItems: boolean): void {
    this.jJGridService.onSelectAllItemsChanged(selectedAllItems);
  }

  onToggleItemSelected(item: any, isSelected: boolean): void {
    this.jJGridService.onToggleItemSelected(item, isSelected);
  }

  isItemSelected(item: any): boolean {
    return !!this.selectedItems.find(si => si.id === item.id);
  }

  getColumnFilterConfig(column: IColumn): Observable<IFilterConfig> {
    return this.filterConfig$.pipe(
      map(confs => confs.find(item => item.prop === column.prop)),
      filter(conf => !!conf),
      take(1)
    );
  }

  onFilterApply(event: IFilterChangedEvent): void {
    this.jJGridService.onFilterApply(event);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
