import {
  Component,
  Input,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IFilterConfig } from '../../models/i-filter-config';
import { IFilter } from '../../models/i-filter';
import { Observable, Subject, BehaviorSubject, of, combineLatest } from 'rxjs';
import {
  debounceTime,
  switchMap,
  withLatestFrom,
  tap,
  distinctUntilChanged,
  map,
  startWith
} from 'rxjs/operators';

@Component({
  selector: 'jj-grid-filter-multiselect',
  templateUrl: 'filter-multi-select.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JJGridFitlerMultiSelectComponent implements OnInit {
  @Input() filterConfig: IFilterConfig;
  @Input() filter: IFilter;

  @Output() filterApply: EventEmitter<IFilter> = new EventEmitter<IFilter>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();

  private optionsObs$: Observable<any[]>;
  options: any[];

  private selectedOptions$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(
    []
  );
  private selectedOptionsObs$: Observable<
    any[]
  > = this.selectedOptions$.asObservable().pipe(distinctUntilChanged());

  private debounceMs: number = 500;
  private searchSubject$: Subject<string> = new Subject<string>();
  searchSubjectObs$: Observable<
    string
  > = this.searchSubject$.asObservable().pipe(debounceTime(this.debounceMs));

  isSelectedAllItemsIndeterminate$: Observable<boolean>;
  isSelectedAllChecked$: Observable<boolean>;

  private get updatedFilter(): IFilter {
    return {
      ...this.filter,
      data: this.selectedOptions$.value
    };
  }

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    const filtersData = [...this.filter.data];
    this.selectedOptions$.next(filtersData);

    this.optionsObs$ = this.searchSubjectObs$.pipe(
      switchMap(search => this.loadOptions(search)),
      startWith(filtersData)
    );

    this.optionsObs$
      .pipe(
        withLatestFrom(this.selectedOptionsObs$),
        tap(([options, _]) => {
          this.options = options;
        }),
        tap(([options, selectedOptions]) => {
          selectedOptions = selectedOptions.filter(
            so => !!options.find(o => o.id === so.id)
          );
          this.selectedOptions$.next(selectedOptions);
        })
      )
      .subscribe();

    const optionStateChange$ = combineLatest(
      this.optionsObs$,
      this.selectedOptionsObs$
    );

    this.isSelectedAllItemsIndeterminate$ = optionStateChange$.pipe(
      map(([options, selectedOptions]) => {
        return (
          selectedOptions.length > 0 &&
          selectedOptions.length !== options.length
        );
      })
    );

    const selectedAllItems$ = optionStateChange$.pipe(
      map(([options, selectedOptions]) => {
        return (
          selectedOptions.length > 0 &&
          selectedOptions.length === options.length
        );
      })
    );

    this.isSelectedAllChecked$ = combineLatest(
      selectedAllItems$,
      this.isSelectedAllItemsIndeterminate$
    ).pipe(map(([sa, intermediate]) => sa || intermediate));
  }

  onSelectAllItemsChanged(selectedAllItems: boolean): void {
    if (selectedAllItems) {
      this.selectedOptions$.next([...this.options]);
    } else {
      this.selectedOptions$.next([]);
    }
  }

  onToggleItemSelected(option: any, isSelected: boolean): void {
    const currentlySelectedOptions = this.selectedOptions$.value;
    const selectedOptions = isSelected
      ? [...currentlySelectedOptions, option]
      : currentlySelectedOptions.filter(item => item.id !== option.id);
    this.selectedOptions$.next(selectedOptions);
  }

  isItemSelected(option: any): boolean {
    return !!this.selectedOptions$.value.find(si => si.id === option.id);
  }

  onSearchChange(search: string): void {
    this.searchSubject$.next(search);
  }

  onFilterApply(): void {
    this.filterApply.emit(this.updatedFilter);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  clearFilters() {
    this.onSearchChange('');
  }

  private loadOptions(search: string): Observable<any[]> {
    if (!search) {
      return of([]);
    }

    if (
      !this.filterConfig.optionsProvider.factory &&
      !this.filterConfig.optionsProvider.url
    ) {
      console.error(
        'Nor options factory neather api url provided for filter ',
        this.filterConfig.optionsProvider
      );
      return of([]);
    }

    if (this.filterConfig.optionsProvider.factory) {
      return this.filterConfig.optionsProvider.factory(
        this.updatedFilter,
        search
      );
    }

    return this.http.post<any[]>(this.filterConfig.optionsProvider.url, {
      filter: this.updatedFilter,
      search
    });
  }
}
