import {
  Component,
  OnDestroy,
  OnInit,
  Input,
  EventEmitter,
  Output,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IPageChangedEvent } from '../../models/i-page-changed-event';

@Component({
  selector: 'jj-grid-pagination',
  templateUrl: 'pagination.html'
})
export class JJGridPaginationComponent implements OnInit, OnChanges, OnDestroy {
  @Input() pageNumber: number = 0;
  @Input() totalItems: number;
  @Input() pageSize: number;
  @Output() pageChanged: EventEmitter<IPageChangedEvent> = new EventEmitter<
    IPageChangedEvent
  >();

  totalPageCount: number = 0;
  pages: number[] = [];
  goToPage: number = null;
  paginationForm: FormGroup;

  private subscriptions: Subscription[] = [];

  constructor() {}

  ngOnInit() {
    this.paginationForm = new FormGroup({
      goToPage: new FormControl('', [
        Validators.pattern('[0-9]*'),
        Validators.min(1)
      ])
    });
    this.calculate();
  }

  ngOnChanges(_: SimpleChanges) {
    this.calculate();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  goToPageNumber(pageNumber: number) {
    if (pageNumber < 0) {
      pageNumber = 0;
    } else if (pageNumber >= this.pages.length) {
      pageNumber = this.pages.length - 1;
    }

    this.pageChanged.emit({ pageNumber });
  }

  isPrevPageAvailable(pageNumber: number): boolean {
    return pageNumber > 0;
  }

  isNextPageAvailable(totalPageCount: number, pageNumber: number): boolean {
    return pageNumber < totalPageCount - 1;
  }

  getInvalidControlStyle(ctrlName: string): any {
    return this.isControlValid(ctrlName) ? {} : { 'border-color': 'red' };
  }

  isControlValid(ctrlName: string): boolean {
    const ctrl = this.paginationForm.controls[ctrlName];
    return !(ctrl.errors && (ctrl.dirty || ctrl.touched));
  }

  onFormSubmit() {
    if (!this.paginationForm.valid) {
      return false;
    }
    this.goToPageNumber(this.paginationForm.controls['goToPage'].value - 1);
  }

  private calculate(): void {
    this.totalPageCount = this.getTotalPageCount(
      this.totalItems,
      this.pageSize
    );
    this.pages = this.getPages(this.totalPageCount);
  }

  private getTotalPageCount(totalItems: number, pageSize: number): number {
    return Math.ceil(totalItems / pageSize);
  }

  private getPages(totalPageCount: number): number[] {
    const result = [];
    for (let i = 0; i < totalPageCount; i++) {
      result.push(i + 1);
    }
    return result;
  }
}
