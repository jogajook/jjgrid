import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from '@angular/core';
import { Subscription } from 'rxjs';
import { IColumn } from '../../models/i-column';
import { IParams } from '../../models/i-params';
import { ESortOrder } from '../../models/e-sort-order';
import { ISortingChangeEvent } from '../../models/i-sorting-change-event';
import { IFilterConfig } from '../../models';
import { IFilter } from '../../models/i-filter';
import { IFilterChangedEvent } from '../../models/i-filter-changed-event';

@Component({
  selector: 'jj-grid-column',
  templateUrl: 'column.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JJGridColumnComponent implements OnInit, OnDestroy {
  @Input() column: IColumn;
  @Input() params: IParams;
  @Input() filterConfig: IFilterConfig;
  @Output() sortingChange: EventEmitter<ISortingChangeEvent> = new EventEmitter<
    ISortingChangeEvent
  >();
  @Output() filterApply: EventEmitter<IFilterChangedEvent> = new EventEmitter<IFilterChangedEvent>();

  public isFilterOpen: boolean = false;

  private subscriptions: Subscription[] = [];

  constructor() {}

  ngOnInit() {
    console.log('filterConfig', this.filterConfig);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onTitleClick(column: IColumn) {
    if (this.filterConfig) {
      this.toggleColumnFilter(true);
    } else {
      this.onSortingChange(column);
    }
  }

  onSortingChange(column: IColumn, sorting?: ESortOrder) {
    if (!column.isSortable) {
      return false;
    }
    if (typeof sorting === 'undefined') {
      sorting =
        this.params.sortOrder === ESortOrder.Ascending &&
        this.params.sortBy === column
          ? ESortOrder.Descending
          : ESortOrder.Ascending;
    }
    this.sortingChange.emit({ column, sorting });
  }

  isSortingAsc(column: IColumn): boolean {
    return (
      this.params.sortBy === column &&
      this.params.sortOrder === ESortOrder.Ascending
    );
  }

  isSortingDesc(column: IColumn): boolean {
    return (
      this.params.sortBy === column &&
      this.params.sortOrder === ESortOrder.Descending
    );
  }

  getColumnFilter(): IFilter {
    return this.params.filters.find(
      item => item.prop === this.filterConfig.prop
    );
  }

  toggleColumnFilter(state: boolean) {
    this.isFilterOpen = state;
  }

  onFilterApply(event: IFilterChangedEvent): void {
    this.toggleColumnFilter(false);
    this.filterApply.emit({...event, column: this.column });
  }
}
