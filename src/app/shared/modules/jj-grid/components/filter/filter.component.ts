import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { IFilterConfig, ESortOrder } from '../../models';
import { EFilterType } from '../../models/e-filter-type';
import { IFilter } from '../../models/i-filter';
import { IFilterChangedEvent } from '../../models/i-filter-changed-event';

@Component({
  selector: 'jj-grid-filter',
  templateUrl: './filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    '.jj-grid-filter { position: absolute; background-color: #ffffff; border: 1px solid #000000; padding-top: 20px; }',
    '.jj-grid-sorting { cursor: pointer; }'
  ]
})
export class JJGridFilterComponent implements OnInit {
  @Input() filterConfig: IFilterConfig;
  @Input() isSortingAsc: boolean;
  @Input() isSortingDesc: boolean;
  @Input() filter: IFilter;
  @Output() filterApply: EventEmitter<IFilterChangedEvent> = new EventEmitter<
    IFilterChangedEvent
  >();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() sortingChange: EventEmitter<ESortOrder> = new EventEmitter<
    ESortOrder
  >();

  filterType = EFilterType;
  sortOrders = [
    {
      title: 'A-Z',
      order: ESortOrder.Ascending,
      className: 'fa-sort-up'
    },
    {
      title: 'Z-A',
      order: ESortOrder.Descending,
      className: 'fa-sort-down'
    }
  ];
  eSortOrder = ESortOrder;
  sorting: ESortOrder = null;

  constructor() {}

  ngOnInit() {}

  onFilterApply(filter: IFilter): void {
    const sorting = this.sorting;
    this.sorting = null;
    this.filterApply.emit({ filter, sorting, column: null });
  }

  onSortingChange(sorting: ESortOrder): void {
    this.sorting = sorting;
    // this.sortingChange.emit(sorting);
  }

  onCancel(): void {
    this.cancel.emit();
  }

  isSortingActive(sortOrder: { order: ESortOrder }): boolean {
    if (this.sorting !== null) {
      return this.sorting === sortOrder.order;
    }
    if (!this.isSortingAsc && !this.isSortingDesc) {
      return false;
    }
    return (
      (this.isSortingAsc && sortOrder.order === ESortOrder.Ascending) ||
      (this.isSortingDesc && sortOrder.order === ESortOrder.Descending)
    );
  }
}
