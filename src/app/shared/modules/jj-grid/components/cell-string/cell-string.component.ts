import {
  Component,
  Input,
  OnInit,
  ChangeDetectionStrategy
} from '@angular/core';
import { IColumn } from '../../models/i-column';

@Component({
  selector: 'jj-grid-cell-string',
  templateUrl: 'cell-string.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JJGridCellStringComponent implements OnInit {
  @Input() column: IColumn;
  @Input() item: any;

  constructor() {}

  ngOnInit() {}
}
