import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import * as fromReducers from './store';
import * as fromComponents from './components';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    AngularFontAwesomeModule,
    StoreModule.forRoot(fromReducers.reducers),
    EffectsModule.forRoot([])
  ],
  exports: [fromComponents.JJGRID_COMPONENTS],
  declarations: [fromComponents.JJGRID_COMPONENTS]
})
export class JJGridModule {}
