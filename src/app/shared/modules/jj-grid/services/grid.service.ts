import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import * as actions from '../store/grid-actions';
import { filter, withLatestFrom, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../store/i-state';
import { IDataSource } from '../models/i-data-source';
import { DataSourceReadyAction } from '../store/grid-actions';
import { getSpecificGridStateSelectors } from '../store';
import { IParams } from '../models/i-params';
import { ISortingChangeEvent } from '../models/i-sorting-change-event';
import { ICellClickEvent } from '../models/i-cell-click-event';
import { IInvokedItemActionEvent } from '../models/i-invoked-item-action-event';
import { IPageChangedEvent } from '../models/i-page-changed-event';
import { IColumn } from '../models/i-column';
import { IGridSelectors } from '../store/i-grid-selectors';
import { IGridAction } from '../models/i-grid-action';
import { PublicGridActionType, IFilterConfig } from '../models';
import { IGridConfig } from '../models/i-grid-config';
import { IFilter } from '../models/i-filter';
import { IFilterChangedEvent } from '../models/i-filter-changed-event';

@Injectable()
export class GridService {
  private selectors: IGridSelectors;
  private gridId: string;

  constructor(private actions$: Actions, private store: Store<State>) {}

  getPublicActions(): Observable<IGridAction> {
    return this.actions$.pipe(
      filter(
        (action: actions.GridActions) =>
          action.gridId === this.gridId && this.isActionPublic(action)
      )
    );
  }

  isActionPublic(action: actions.GridActions): boolean {
    return typeof PublicGridActionType[action.type] !== 'undefined';
  }

  registerGrid(source: IDataSource): void {
    this.selectors = getSpecificGridStateSelectors(source.id);
    this.gridId = source.id;
    this.store.dispatch(new DataSourceReadyAction(source, source.id));
  }

  // store methods
  getDataSource(): Observable<IDataSource> {
    return this.store.select<IDataSource>(this.selectors.getDataSource);
  }

  getColumns(): Observable<IColumn[]> {
    return this.store.select<IColumn[]>(this.selectors.getColumns);
  }

  getParams(): Observable<IParams> {
    return this.store.select<IParams>(this.selectors.getParams);
  }

  getPageSize(): Observable<number> {
    return this.store.select<number>(this.selectors.getPageSize);
  }

  getConfig(): Observable<IGridConfig> {
    return this.store.select<IGridConfig>(this.selectors.getConfig);
  }

  getPageNumber(): Observable<number> {
    return this.store.select<number>(this.selectors.getPageNumber);
  }

  getDataTotalItems(): Observable<number> {
    return this.store.select<number>(this.selectors.getDataTotalItems);
  }

  getDataItems(): Observable<any> {
    return this.store.select<any>(this.selectors.getDataItems);
  }

  getSearch(): Observable<string> {
    return this.store.select<string>(this.selectors.getSearch);
  }

  getSelectedItems(): Observable<any[]> {
    return this.store.select<any>(this.selectors.getSelectedItems);
  }

  getSelectedAllItems(): Observable<boolean> {
    return this.store.select<any>(this.selectors.getSelectedAllItems);
  }

  isSelectedAllItemsIndeterminate(): Observable<boolean> {
    return this.getSelectedItems().pipe(
      withLatestFrom(this.getDataItems()),
      map(([selectedItems, items]) => {
        return selectedItems.length > 0 && selectedItems.length < items.length;
      })
    );
  }

  getFilterConfig(): Observable<IFilterConfig[]> {
    return this.getConfig().pipe(map(config => config.filterConfig));
  }

  onSearchChange(search: string): void {
    this.store.dispatch(new actions.SearchChangedAction(search, this.gridId));
  }

  onSortingChange(sortingChangeEvent: ISortingChangeEvent): void {
    this.store.dispatch(
      new actions.SortingChangedAction(sortingChangeEvent, this.gridId)
    );
  }

  onCellClick(cellClickEvent: ICellClickEvent): void {
    this.store.dispatch(
      new actions.CellClickedAction(cellClickEvent, this.gridId)
    );
  }

  onInvokedAction(invokedItemActionEvent: IInvokedItemActionEvent): void {
    this.store.dispatch(
      new actions.ItemActionInvokedAction(invokedItemActionEvent, this.gridId)
    );
  }

  onPageChanged(pageChangedEvent: IPageChangedEvent): void {
    this.store.dispatch(
      new actions.PageChangedAction(pageChangedEvent, this.gridId)
    );
  }

  onSelectAllItemsChanged(selectedAllItems: boolean): void {
    this.store.dispatch(
      new actions.SelectAllItemsChangedAction(selectedAllItems, this.gridId)
    );
  }

  onToggleItemSelected(item: any, isSelected: boolean): void {
    this.store.dispatch(
      new actions.ToggleItemSelectedAction({ item, isSelected }, this.gridId)
    );
  }

  onFilterApply(event: IFilterChangedEvent): any {
     this.store.dispatch(
      new actions.FilterChangedAction(event, this.gridId)
    );
  }
}
