import { IActionConfigItem } from './i-action-config-item';

export interface IColumnActionConfig {
  getFilter: (item: any) => boolean;
  actions: IActionConfigItem[];
}
