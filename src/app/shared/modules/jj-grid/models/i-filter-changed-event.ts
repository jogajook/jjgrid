import { IFilter } from './i-filter';
import { ESortOrder } from './e-sort-order';
import { ISortingChangeEvent } from './i-sorting-change-event';

export interface IFilterChangedEvent extends ISortingChangeEvent {
   filter: IFilter;
}
