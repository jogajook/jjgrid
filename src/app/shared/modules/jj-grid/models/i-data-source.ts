import { IParams } from './i-params';
import { IData } from './i-data';
import { IColumn } from './i-column';
import { Observable } from 'rxjs';
import { IGridConfig } from './i-grid-config';

export interface IDataSource<T = any> {
  id: string;
  columns: IColumn[];
  params: IParams;
  config: IGridConfig;
  data: IData<T>;
  loadData();
  getData(params: IParams): Observable<IData<T>>;
}

