import { EColumnType } from './e-column-type';

export interface IColumn {
  type: EColumnType;
  prop: string;
  title: string;
  isSortable: boolean;
}
