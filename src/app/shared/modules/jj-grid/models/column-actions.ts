import { IColumnActionConfig } from './i-column-action-config';
import { EColumnType } from './e-column-type';
import { Column } from './column';

export class ColumnActions extends Column {
  constructor(
    public type: EColumnType,
    public prop: string,
    public title: string,
    public isSortable: boolean = true,
    public actionConfig: IColumnActionConfig
  ) {
    super(type, prop, title, isSortable);
  }
}
