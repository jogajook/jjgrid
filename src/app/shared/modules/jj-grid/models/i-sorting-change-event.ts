import { IColumn } from './i-column';
import { ESortOrder } from './e-sort-order';

export interface ISortingChangeEvent {
   column: IColumn;
   sorting: ESortOrder;
  }
