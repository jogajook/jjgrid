
export const PublicGridActionType = {
  CellClicked: 'CellClicked',
  ItemActionInvoked: 'ItemActionInvoked'
};

export const PrivateGridActionType = {
  DataSourceReady: 'DataSourceReady',
  SortingChanged: 'SortingChanged',
  SearchChanged: 'SearchChanged',
  PageChanged: 'PageChanged',
  PageSizeChanged: 'PageSizeChanged',
  SelectAllItemsChanged: 'SelectAllItemsChanged',
  ToggleItemSelected: 'ToggleItemSelected',
  FilterChanged: 'FilterChanged',
};

export const GridActionType = {
  ...PrivateGridActionType,
  ...PublicGridActionType
};

