import { IFilterConfig } from './i-filter-config';

export interface IGridConfig {
  isSelectable: boolean;
  filterConfig: IFilterConfig[];
}
