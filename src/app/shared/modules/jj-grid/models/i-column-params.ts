import { IColumnActionConfig } from './i-column-action-config';

export interface IColumnParams {
  prop?: string;
  title: string;
  isSortable?: boolean;
  actionConfig?: IColumnActionConfig;
}
