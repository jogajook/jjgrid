export default class Utils {
  static copyInstance(source: any): any {
    const proto = Object.getPrototypeOf(source);
    // tslint:disable-next-line:no-shadowed-variable
    const descriptors = Object.keys(source).reduce((descriptors, key) => {
      descriptors[key] = Object.getOwnPropertyDescriptor(source, key);
      return descriptors;
    }, {});
    return Object.create(proto, descriptors);
  }
}
