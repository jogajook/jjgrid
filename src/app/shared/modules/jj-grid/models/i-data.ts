export interface IData<T = any> {
  page: number;
  totalItems: number;
  items: T[];
}
