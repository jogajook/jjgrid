import { IColumn } from './i-column';
import { EColumnType } from './e-column-type';
import { IColumnParams } from './i-column-params';
import { ColumnActions } from './column-actions';
import { Column } from './column';

export class ColumnFactory {
  static create(type: EColumnType, params: IColumnParams): IColumn {
    let column: IColumn;

    switch (type) {
      case EColumnType.Actions:
        column = new ColumnActions(
          EColumnType.Actions,
          '',
          params.title,
          false,
          params.actionConfig
        );
        break;

      default:
        column = new Column(type, params.prop, params.title, params.isSortable);
    }

    return column;
  }
}
