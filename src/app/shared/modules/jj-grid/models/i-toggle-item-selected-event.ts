export interface IToggleItemSelectedEvent {
   item: any;
   isSelected: boolean;
}
