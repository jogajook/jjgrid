import { IColumn } from './i-column';
import { EColumnType } from './e-column-type';

export class Column implements IColumn {
  constructor(
    public type: EColumnType,
    public prop: string,
    public title: string,
    public isSortable: boolean = true
  ) {}
}
