import { IColumnActionConfig } from './i-column-action-config';
import { IColumn } from './i-column';

export interface IColumnActions extends IColumn {
  actionConfig: IColumnActionConfig;
}
