import { ESortOrder } from './e-sort-order';
import { IParams } from './i-params';
import { IColumn } from './i-column';

export class Params implements IParams {
  constructor(
    public sortBy: IColumn = null,
    public sortOrder: ESortOrder = null,
    public search = '',
    public pageNumber = 0,
    public pageSize = 50,
    public filters = [],
  ) {}
}
