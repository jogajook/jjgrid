import { IActionConfigItem } from './i-action-config-item';

export interface IInvokedItemAction {
  action: IActionConfigItem;
  item: any;
}
