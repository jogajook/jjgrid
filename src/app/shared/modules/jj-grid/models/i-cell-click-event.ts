import { IColumn } from './i-column';

export interface ICellClickEvent<T = any> {
   item: T;
   column: IColumn;
}
