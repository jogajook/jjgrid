export interface IPageChangedEvent {
   pageNumber: number;
}
