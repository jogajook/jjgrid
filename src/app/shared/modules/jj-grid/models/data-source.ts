import { IData } from './i-data';
import { IDataSource } from './i-data-source';
import { IColumn } from './i-column';
import { IParams } from './i-params';
import { Params } from './params';
import { Observable } from 'rxjs';

export abstract class DataSource<T = any> implements IDataSource<T> {
  data: IData<T>;

  constructor(
    public columns: IColumn[] = [],
    public params: IParams = new Params(),
    public id: string,
    public config: any
  ) {}

  abstract getData(params: IParams): Observable<IData<T>>;

  loadData() {
    this.getData(this.params).subscribe((data: IData<T>) => {
      this.data = data;
    });
  }
}
