export interface IInvokedItemActionEvent<T = any, R = any> {
   item: T;
   action: R;
}
