import { ESortOrder } from './e-sort-order';
import { IColumn } from './i-column';
import { IFilter } from './i-filter';

export interface IParams {
  sortBy: IColumn;
  sortOrder: ESortOrder;
  search: string;
  pageNumber: number;
  pageSize: number;
  filters: IFilter[];
}
