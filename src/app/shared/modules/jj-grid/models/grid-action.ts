import { IGridAction } from './i-grid-action';

export class GridAction implements IGridAction {
  type: string;
  gridId: string;
}
