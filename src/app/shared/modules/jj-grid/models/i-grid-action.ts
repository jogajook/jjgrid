import { Action } from '@ngrx/store';

export interface IGridAction extends Action {
  payload?: any;
  gridId: string;
}
