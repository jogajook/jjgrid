import { IFilter } from './i-filter';
import { Observable } from 'rxjs';

export interface IFilterConfig {
  prop: string;
  type: any;
  optionsProvider: {
    url?: string;
    factory?: (filter: IFilter, search: string) => Observable<any[]>;
  };
}
